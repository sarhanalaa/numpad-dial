<?php

namespace Sarhan\NumpadDial;

/**
 * @inheritdoc
 */
abstract class BaseGenerator implements ValidDialGenerator
{
	const VALID_NEIGHBOUR = [
		1 => [2 , 4],
		2 => [1, 3, 5],
		3 => [2, 6],
		4 => [1, 5, 7],
		5 => [4, 2, 6, 8],
		6 => [5, 3, 9],
		7 => [4, 8],
		8 => [7, 5, 9, 0],
		9 => [8, 6],
		0 => [8]
	];

	/**
	 * Asserts generator arguments validity.
	 * 
	 * @param int $start
	 * @param int $length
	 * 
	 * @return void
	 * @throws \InvalidArgumentException when an argument is invalid
	 */
	public function assertValidArguments(int $start, int $length)
	{
		if ($start < 0 || $start > 9) {
			throw new \InvalidArgumentException('first argument must be between 0 and 9.');
		}

		if ($length < 1) {
			throw new \InvalidArgumentException('second argument must be equal to or bigger than 1.');
		}
	}

	/**
	 * Returns valid neighbour numbers to given number.
	 * 
	 * Implementations must use this function to get valid neighbours at each step,
	 * and must yield results traversed in the same order as returned by this function.
	 * 
	 * Example: 1 has 2 and 4 as neighbours. when calculating solutions for 1
	 * as a starting point, 12... series must be yielded before 14... series.
	 * 
	 * @param int $num
	 * @return array
	 */
	public function getNeighbours(int $num)
	{
		return self::VALID_NEIGHBOUR[$num] ?? [];
	}
}