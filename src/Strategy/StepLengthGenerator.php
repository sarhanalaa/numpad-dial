<?php

namespace Sarhan\NumpadDial\Strategy;

use Sarhan\NumpadDial\BaseGenerator;

/**
 * @inheritdoc
 * 
 * A step-length dynamic programming implementation of ValidDialGenerator
 */
final class StepLengthGenerator extends BaseGenerator
{
	private $stepLength;

	public function __construct($stepLength = 3)
	{
		if ($stepLength < 3) {
			throw new \InvalidArgumentException('stepLength must be at least 3');
		}

		$this->recursiveGenerator = new RecursiveGenerator();
		$this->stepLength = $stepLength;
		$this->computed = [];

		foreach (self::VALID_NEIGHBOUR as $dial => $neighbours) {
			$this->computed[$dial] = [1 => [[$dial]]];
		}
	}

	/**
	 * @inheritdoc
	 */
	public function create(int $start, int $length) : \Generator
	{
		$this->assertValidArguments($start, $length);

		if ($length > $this->stepLength) {
			foreach($this->create($start, $this->stepLength) as $firstPart) {
				foreach($this->getNeighbours(end($firstPart)) as $neighbour) {
					foreach($this->create($neighbour, $length - $this->stepLength) as $secondPart) {
						yield array_merge($firstPart, $secondPart);
					}
				}
			}
		} else {
			if (!isset($this->computed[$start][$length])) {
				foreach($this->recursiveGenerator->create($start, $length) as $res) {
					$this->computed[$start][$length][] = $res;
					yield $res;
				}
			} else {
				yield from $this->computed[$start][$length];
			}
		}
	}
}
