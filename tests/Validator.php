<?php

namespace Sarhan\NumpadDial\Tests;

use Sarhan\NumpadDial\BaseGenerator;

class Validator
{
	public function validate($dial)
	{
		if (empty($dial)) {
			return false;
		}

		for ($i = 1; $i < count($dial); $i++)
		{
			if (!in_array($dial[$i], BaseGenerator::VALID_NEIGHBOUR[$dial[$i-1]])) {
				return false;
			}
		}

		return true;
	}
}