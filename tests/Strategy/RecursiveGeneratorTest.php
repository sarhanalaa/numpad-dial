<?php

namespace Sarhan\NumpadDial\Tests\Strategy;

use Sarhan\NumpadDial\Tests\GeneratorTest;
use Sarhan\NumpadDial\ValidDialGenerator;
use Sarhan\NumpadDial\Strategy\RecursiveGenerator;
use Sarhan\NumpadDial\Tests\Validator;

class RecursiveGeneratorTest extends GeneratorTest
{
	protected function setUpGenerator() : ValidDialGenerator
	{
		return new RecursiveGenerator();
	}
}