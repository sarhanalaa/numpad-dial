<?php

namespace Sarhan\NumpadDial\Tests\Strategy;

use Sarhan\NumpadDial\Tests\GeneratorTest;
use Sarhan\NumpadDial\ValidDialGenerator;
use Sarhan\NumpadDial\Strategy\StackGenerator;
use Sarhan\NumpadDial\Tests\Validator;

class StackGeneratorTest extends GeneratorTest
{
	protected function setUpGenerator() : ValidDialGenerator
	{
		return new StackGenerator();
	}
}