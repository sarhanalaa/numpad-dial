<?php

namespace Sarhan\NumpadDial;

/**
 * Generates unique valid dial numbers of given length and starting
 * from given inital position.
 * 
 * <p>
 * A valid dial number is one that has every two consecutive digits next to
 * each other, either vertically or horizontally, on a standard phone dial pad:
 * <pre>
 * 1 2 3
 * 4 5 6
 * 7 8 9
 *   0
 * </pre>
 * </p>
 * 
 * <em>
 * Loop back neighbouring is not permitted. 3 cannot loop back to 1.
 * </em>
 * 
 */
interface ValidDialGenerator
{
	/**
	 * Creates a generator that yields unique valid dials of lenght
	 * `length` and starting from `start`.
	 * 
	 * @param int $start must be in range [0, 9], otherwise output is not defined.
	 * @param int $length must be >= 0, otherwise output is not defined.
	 * @return Generator yields unique valid dials for given start and length.
	 */
	public function create(int $start, int $length) : \Generator;
}