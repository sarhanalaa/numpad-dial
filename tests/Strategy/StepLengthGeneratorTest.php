<?php

namespace Sarhan\NumpadDial\Tests\Strategy;

use Sarhan\NumpadDial\Tests\GeneratorTest;
use Sarhan\NumpadDial\ValidDialGenerator;
use Sarhan\NumpadDial\Strategy\StepLengthGenerator;
use Sarhan\NumpadDial\Tests\Validator;

class StepLengthGeneratorTest extends GeneratorTest
{
	protected function setUpGenerator() : ValidDialGenerator
	{
		return new StepLengthGenerator();
	}
}