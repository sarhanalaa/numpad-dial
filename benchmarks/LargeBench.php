<?php

use Sarhan\NumpadDial\Strategy\RecursiveGenerator;
use Sarhan\NumpadDial\Strategy\StackGenerator;
use Sarhan\NumpadDial\Strategy\StepLengthGenerator;

/**
 * @Iterations(2)
 * @Groups({"large"})
 */
class LargeBench
{
	public function __construct()
	{
		$this->recursiveGenerator = new RecursiveGenerator();
		$this->stackGenerator = new StackGenerator();
		$this->stepLengthGenerator = new StepLengthGenerator();
		$this->stepLengthGenerator4 = new StepLengthGenerator(4);
		$this->stepLengthGenerator5 = new StepLengthGenerator(5);
		$this->stepLengthGenerator6 = new StepLengthGenerator(6);

		$this->start = 5;
		$this->length = 13;
	}

	public function benchRecursiveGenerator()
	{
		iterator_to_array($this->recursiveGenerator->create($this->start, $this->length));
	}

	public function benchStackGenerator()
	{
		iterator_to_array($this->stackGenerator->create($this->start, $this->length));
	}

	public function benchStepLengthGenerator()
	{
		iterator_to_array($this->stepLengthGenerator->create($this->start, $this->length));
	}

	public function benchStepLengthGenerator4()
	{
		iterator_to_array($this->stepLengthGenerator4->create($this->start, $this->length));
	}

	public function benchStepLengthGenerator5()
	{
		iterator_to_array($this->stepLengthGenerator5->create($this->start, $this->length));
	}

	public function benchStepLengthGenerator6()
	{
		iterator_to_array($this->stepLengthGenerator6->create($this->start, $this->length));
	}
}