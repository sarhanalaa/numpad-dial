<?php

namespace Sarhan\NumpadDial\Strategy;

use Sarhan\NumpadDial\BaseGenerator;

/**
 * @inheritdoc
 * 
 * An iterative stack-based algorithm to generate unique valid dial numbers.
 */
final class StackGenerator extends BaseGenerator
{
	/**
	 * @inheritdoc
	 */
	public function create(int $start, int $length) : \Generator
	{
		$this->assertValidArguments($start, $length);

		if ($length === 1) {
			yield [$start];
			return;
		}

		$dialStack = new \SplStack();
		$movesStack = new \SplStack();
		$movesCountStack = new \SplStack();

		$startMoves = $this->getNeighbours($start);

		$dialStack->push($start);
		$this->pushMovesIntoStack($movesStack, $startMoves);
		$movesCountStack->push(count($startMoves));

		while($movesStack->count() > 0)
		{
			$dialStack->push($movesStack->pop());
			$movesCountStack->push($movesCountStack->pop() - 1);

			if ($dialStack->count() < $length) {
				$nextMoves = $this->getNeighbours($dialStack->top());
				$this->pushMovesIntoStack($movesStack, $nextMoves);
				$movesCountStack->push(count($nextMoves));
			} else {
				yield $this->stackToArray($dialStack);

				$dialStack->pop();

				while($movesCountStack->count() && $movesCountStack->top() <= 0) {
					$dialStack->pop();
					$movesCountStack->pop();
				}
			}
		}
	}

	private function stackToArray(\SplStack $stack)
	{
		$res = [];
		foreach($stack as $num) {
			$res[] = $num;
		}

		return array_reverse($res);
	}

	private function pushMovesIntoStack(\SplStack $stack, array $moves)
	{
		foreach(array_reverse($moves) as $move)
		{
			$stack->push($move);
		}
	}
}