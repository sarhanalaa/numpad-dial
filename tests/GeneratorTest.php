<?php

namespace Sarhan\NumpadDial\Tests;

use PHPUnit\Framework\TestCase;
use Sarhan\NumpadDial\ValidDialGenerator;
use Sarhan\NumpadDial\Tests\Validator;

abstract class GeneratorTest extends TestCase
{
	public function setUp()
	{
		$this->validator = new Validator();
		$this->generator = $this->setUpGenerator();
	}

	protected abstract function setUpGenerator() : ValidDialGenerator;

	public function invalidInputProvider()
	{
		return [
			[-1, 1],
			[10, 1],
			[1, 0],
			[1, -1]
		];
	}

	/**
	 * @dataProvider invalidInputProvider
	 */
	public function testGeneratePanicsForInvalidInput($start, $length)
	{
		$this->expectException(\InvalidArgumentException::class);

		$this->generator->create($start, $length)->next();
	}

	public function generatorDataProvider()
	{
		return [
			[
				5,
				1,
				['5']
			],
			[
				1,
				3,
				['121', '123', '125', '141', '145', '147']
			],
			[
				5,
				3,
				[
					'541', '545', '547', '521', '523', '525', '565',
					'563', '569', '587', '585', '589', '580'
				]
			],
			[
				1,
				4,
				[
					'1212', '1214', '1232', '1236', '1254', '1252', '1256',
					'1258', '1412', '1414', '1454', '1452', '1456', '1458',
					'1474', '1478'
				]
			],
			[
				0,
				4,
				[
					'0874', '0878', '0854', '0852', '0856', '0858', '0898',
					'0896', '0808'
				]
			]
		];
	}

	/**
	 * @dataProvider generatorDataProvider
	 */
	public function testGenerator($start, $length, $expectedResultsAsStrings)
	{
		$resultsAsStrings = [];
		foreach ($this->generator->create($start, $length) as $result) {
			$resultsAsStrings[] = implode('', $result);
		}

		$this->assertEquals($expectedResultsAsStrings, $resultsAsStrings);
	}

	public function stressDataProvider()
	{
		return [
			[1, 10],
			[0, 10],
			[2, 20],
			[4, 30],
			[9, 40],
		];
	}

	/**
	 * @group stress
	 * @dataProvider stressDataProvider
	 */
	public function testGeneratorStress($start, $length)
	{
		$generator = $this->generator->create($start, $length);

		// 10000 is an upper limit of generated and validated dials in this test
		for ($i = 0; $i < 10000 && $generator->valid(); $i++) {
			$dial = $generator->current();
			$generator->next();

			$this->assertTrue($this->validator->validate($dial));
		}
	}
}