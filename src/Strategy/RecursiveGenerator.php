<?php

namespace Sarhan\NumpadDial\Strategy;

use Sarhan\NumpadDial\BaseGenerator;

/**
 * @inheritdoc
 * 
 * A simple recrusive algorithm to generate unique valid dial numbers.
 */
final class RecursiveGenerator extends BaseGenerator
{
	/**
	 * @inheritdoc
	 */
	public function create(int $start, int $length) : \Generator
	{
		$this->assertValidArguments($start, $length);

		if ($length === 1) {
			yield [$start];
		} else {
			foreach($this->getNeighbours($start) as $neighbour) {
				foreach ($this->create($neighbour, $length - 1) as $neighbourDial) {
					yield array_merge([$start], $neighbourDial);
				}
			}
		}
	}
}