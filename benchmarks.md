Numpad Dial Generator Benchmarks
================================

### groups: large

benchmark | subject | params | revs | its | mem_peak | best | mean | mode | worst | stdev | rstdev | diff
 --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- 
LargeBench | benchRecursiveGenerator | [] | 1 | 2 | 371,804,152b | 23,379,633.000μs | 23,514,134.500μs | 23,513,871.288μs | 23,648,636.000μs | 134,501.500μs | 0.57% | 6.74x
LargeBench | benchStackGenerator | [] | 1 | 2 | 371,790,424b | 17,056,195.000μs | 17,182,422.000μs | 17,182,669.020μs | 17,308,649.000μs | 126,227.000μs | 0.73% | 4.93x
LargeBench | benchStepLengthGenerator | [] | 1 | 2 | 293,232,512b | 8,539,597.000μs | 8,551,316.000μs | 8,551,338.933μs | 8,563,035.000μs | 11,719.000μs | 0.14% | 2.45x
LargeBench | benchStepLengthGenerator4 | [] | 1 | 2 | 231,148,368b | 5,632,843.000μs | 5,694,727.000μs | 5,694,605.896μs | 5,756,611.000μs | 61,884.000μs | 1.09% | 1.63x
LargeBench | benchStepLengthGenerator5 | [] | 1 | 2 | 372,003,240b | 3,483,776.000μs | 3,487,160.000μs | 3,487,153.378μs | 3,490,544.000μs | 3,384.000μs | 0.10% | 1.00x
LargeBench | benchStepLengthGenerator6 | [] | 1 | 2 | 286,846,648b | 5,973,384.000μs | 5,985,731.000μs | 5,985,706.838μs | 5,998,078.000μs | 12,347.000μs | 0.21% | 1.72x

### groups: medium

benchmark | subject | params | revs | its | mem_peak | best | mean | mode | worst | stdev | rstdev | diff
 --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- 
MediumBench | benchRecursiveGenerator | [] | 1 | 5 | 2,174,048b | 74,224.000μs | 74,708.200μs | 74,339.618μs | 75,665.000μs | 576.599μs | 0.77% | 8.44x
MediumBench | benchStackGenerator | [] | 1 | 5 | 2,167,080b | 77,459.000μs | 77,750.000μs | 77,519.127μs | 78,688.000μs | 471.328μs | 0.61% | 8.78x
MediumBench | benchStepLengthGenerator | [] | 1 | 5 | 2,201,856b | 23,192.000μs | 23,295.600μs | 23,296.479μs | 23,395.000μs | 66.773μs | 0.29% | 2.63x
MediumBench | benchStepLengthGenerator4 | [] | 1 | 5 | 1,956,784b | 8,779.000μs | 8,853.400μs | 8,809.059μs | 9,019.000μs | 88.060μs | 0.99% | 1.00x
MediumBench | benchStepLengthGenerator5 | [] | 1 | 5 | 2,237,192b | 12,846.000μs | 12,907.800μs | 12,941.812μs | 12,966.000μs | 49.548μs | 0.38% | 1.46x
MediumBench | benchStepLengthGenerator6 | [] | 1 | 5 | 2,305,672b | 23,731.000μs | 23,912.400μs | 23,760.634μs | 24,528.000μs | 308.351μs | 1.29% | 2.70x

### groups: small

benchmark | subject | params | revs | its | mem_peak | best | mean | mode | worst | stdev | rstdev | diff
 --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- 
SmallBench | benchRecursiveGenerator | [] | 1 | 10 | 1,234,200b | 2,540.000μs | 2,752.400μs | 2,596.677μs | 4,149.000μs | 468.047μs | 17.01% | 2.39x
SmallBench | benchStackGenerator | [] | 1 | 10 | 1,230,712b | 3,307.000μs | 3,705.400μs | 3,398.556μs | 6,426.000μs | 914.187μs | 24.67% | 3.21x
SmallBench | benchStepLengthGenerator | [] | 1 | 10 | 1,247,472b | 1,123.000μs | 1,152.600μs | 1,136.808μs | 1,249.000μs | 36.949μs | 3.21% | 1.00x
SmallBench | benchStepLengthGenerator4 | [] | 1 | 10 | 1,251,760b | 2,178.000μs | 2,706.800μs | 2,207.290μs | 7,167.000μs | 1,487.033μs | 54.94% | 2.35x
SmallBench | benchStepLengthGenerator5 | [] | 1 | 10 | 1,243,704b | 2,774.000μs | 2,835.800μs | 2,807.816μs | 2,934.000μs | 49.833μs | 1.76% | 2.46x
SmallBench | benchStepLengthGenerator6 | [] | 1 | 10 | 1,243,704b | 2,791.000μs | 2,845.400μs | 2,808.863μs | 2,954.000μs | 58.093μs | 2.04% | 2.47x

