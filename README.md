# numpad-dial
---

A php library to generate valid dialpad numbers as per the following definition:

A standard phone number pad has the following layout

```
1 2 3
4 5 6
7 8 9
  0
```

A valid dialpad number on that layout is defined as a sequence of digits, that
fulfill the following neighbouring rules for every two consecutive digits:

1. the two digits are above each other, eg. 1 and 4; or
2. the two digits are next to each other, eg. 5 and 6.

Loop back neighbouring is not valid. Eg. 3 is not a neighbour of 1 nor 9.
Same for 0, is not a neighbour of 2.

---

## Dependencies

* PHP7
* php_debug (optional) to generate code coverage
* [composer](https://getcomposer.org/download/)

## Setup

1. clone repository

```
git clone git@gitlab.com:sarhanalaa/numpad-dial.git
```

2. install dependencies

```
composer install
```

## Running tests

Code coverage will be generated under `./coverage`.

### Completeness and Correctness tests

```
composer test
```

### Stress tests

```
composer test-stress
```

## Benchmarking

Using [phpbench](https://github.com/phpbench/phpbench)

```
composer bench
```

Benchmarking is done in 3 groups, small (start = 5, length = 5), medium (start = 5, length = 8) and large (start = 5, length = 13).

Reports will be generated at [benchmarks.html](benchmarks.html) and [benchmarks.md](benchmarks.md).